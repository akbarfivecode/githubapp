package com.akbar.githubapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PROFILE = "extra_profile"
    }

    private lateinit var profile: Profile

    private lateinit var photoProfile: ImageView
    private lateinit var titleText: TextView
    private lateinit var descText: TextView
    private lateinit var followersText: TextView
    private lateinit var followingText: TextView
    private lateinit var repoText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        profile = intent.getParcelableExtra<Profile>(EXTRA_PROFILE) as Profile

        photoProfile = findViewById(R.id.img_profile)
        titleText = findViewById(R.id.tv_name2)
        descText = findViewById(R.id.tv_nation)
        followersText = findViewById(R.id.tv_followers)
        followingText = findViewById(R.id.tv_following)
        repoText = findViewById(R.id.tv_repo)
        initView()
    }

    private fun initView() {
        photoProfile.setImageResource(profile.photo)
        titleText.text = profile.name
        descText.text = profile.country
        followersText.text = "${profile.follwers} followers"
        followingText.text = "${profile.following} following"
        repoText.text = "${profile.repo} repository"
        title = profile.username
    }
}