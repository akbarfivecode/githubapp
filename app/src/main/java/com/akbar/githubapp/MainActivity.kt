package com.akbar.githubapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var recycleView: RecyclerView
    private val list = ArrayList<Profile>()

    private val listUser: ArrayList<Profile>
        get() {
            val dataUsername = resources.getStringArray(R.array.username)
            val dataName = resources.getStringArray(R.array.name)
            val dataCountry = resources.getStringArray(R.array.location)
            val datarepo = resources.getStringArray(R.array.repository)
            val dataCompany = resources.getStringArray(R.array.company)
            val dataFollowing = resources.getStringArray(R.array.following)
            val dataFollowers = resources.getStringArray(R.array.followers)
            val dataPhoto = resources.obtainTypedArray(R.array.avatar)
            val listProfile = ArrayList<Profile>()
            for (i in dataUsername.indices) {
                val profile = Profile(
                    dataUsername[i],
                    dataName[i],
                    dataCountry[i],
                    datarepo[i],
                    dataCompany[i],
                    dataFollowing[i],
                    dataFollowers[i],
                    dataPhoto.getResourceId(i, -1)
                )
                listProfile.add(profile)
            }
            return listProfile
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycleView = findViewById(R.id.recycleView)
        recycleView.setHasFixedSize(true)

        list.addAll(listUser)
        showData()
    }

    private fun showData() {
        recycleView.layoutManager = LinearLayoutManager(this)
        val userAdapter = ListAdapter(list) {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailActivity.EXTRA_PROFILE, it)
            startActivity(intent)
        }
        recycleView.adapter = userAdapter

    }
}