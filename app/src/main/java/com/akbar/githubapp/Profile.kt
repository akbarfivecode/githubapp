package com.akbar.githubapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile(
    var username: String,
    var name: String,
    var country: String,
    var repo: String,
    var company: String,
    var follwers: String,
    var following: String,
    var photo: Int
) : Parcelable
